# music

#### 介绍
** 该项目是Docker-compose 个人练习项目，www 中包含一个PHP 网站 可以用docker来部署该服务**


#### 安装教程
~~~
root@serv# docker-compose up -d nginx
~~~

#### 使用说明
 打开游览器 访问 http://localhost/  即可

#### 特别感谢以下项目
~~~
1   laradock  https://github.com/laradock/laradock
2   * @author  MaiCong <i@maicong.me>
    * @link    https://github.com/maicong/music
~~~