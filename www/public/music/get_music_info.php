<?php
define('MC_CORE',1);
define('MC_CORE_DIR', __DIR__ . '/core');
include_once MC_CORE_DIR . '/music.php';
$music_input          = get('mid');
$music_type           = get('type')??'qq';

$music_response = mc_get_song_by_id($music_input, $music_type='qq');
response($music_response, 200, '');

// GET
function get($key)
{
    return isset($_GET[$key]) ? $_GET[$key] : null;
}